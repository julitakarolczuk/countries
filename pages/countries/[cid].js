import React from 'react'
import Layout from '../../components/Layout'
import MapContainer from '../../components/Map'
import CountryName from '../../components/CountryName'
import CountryDetails from '../../components/CountryDetails'
import Languages from '../../components/Languages'
import Borders from '../../components/Borders'

export default () => {
    return (
        <Layout>
            <div className="details-page">
                <div className="details-wrapper">
                    <div className="box">
                        <CountryName />
                        <CountryDetails />
                    </div>
                    <div className="box">
                        <Languages />
                        <Borders />
                    </div>
                </div>
                <MapContainer />
            </div>
            <style jsx>
                {`
                    .details-page {
                        display: flex;
                        flex-direction: column;
                    }
                    .details-wrapper {
                        display: flex;
                        justify-content: space-between;
                    }
                    .box {
                        mi-width: 30%;
                    }
                `}
            </style>
        </Layout>
    )
}

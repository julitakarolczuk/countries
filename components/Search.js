import React from 'react'
import { useRouter } from 'next/router'
import SearchBox from './SearchBox'

const Search = () => {
    const router = useRouter()
    const { cid } = router.query
    
    return (
        <div>
            <label>
                {cid ? (
                    `Search for more! Type your new destination: `
                ) : (
                    'No countries searched yet, check your dreaming country!'
                )} 
            </label>
            <SearchBox />
        </div>  
    )
}

export default Search

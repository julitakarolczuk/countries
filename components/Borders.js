import React from 'react'
import { CountryContext } from './Layout'

class Borders extends React.Component {    
    static contextType = CountryContext

    render () {
        const {
            selectedCountry,
            selectedCountry: {
                borders = []
            }
        } = this.context

        return JSON.stringify(selectedCountry) !== '{}'
            ? (
                <div>
                    <h4>
                        Borders:
                    </h4>
                    {borders.length 
                        ? (
                            <ul>
                                {borders.map(border => (
                                    <li key={border}>{border}</li>
                                ))}
                            </ul>
                        ) : <p>
                                This country has no borderer.
                            </p>}

                </div>
            ) : null
    }
}

export default Borders
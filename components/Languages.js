import React, { useContext } from 'react'
import { CountryContext, LanguageContext } from './Layout'

const Languages = () => {  
    const {
        selectedCountry: {
            languages = []
        }
    } = useContext(CountryContext)

    const {
        lang
    } = useContext(LanguageContext)

    return (
        <div>
            {languages.length 
                ? (
                    <>
                        <h4>
                            {lang === 'pl' ? 'Języki:' : 'Languages:'}
                        </h4>
                        <ul>
                            {languages.map(language => (
                                <li key={language}>{language}</li>
                            ))}
                        </ul>
                    </>
                ) : null
            }
        </div>
    )
}

export default Languages
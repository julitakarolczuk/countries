import React from 'react'
import { CountryContext } from './Layout'

class CountryDetails extends React.Component {  
    render () {
        const {
            selectedCountry,
            selectedCountry: {
                capital,
                region,
                subregion,
                area,
                population,
                timezones = []
            }
        } = this.context

        return (
            <div>
                {JSON.stringify(selectedCountry) !== '{}'
                    ? (
                        <>
                            <p>
                                Capital: {capital}
                            </p>
                            <p>
                                Region: {region}
                            </p>
                            <p>
                                Subregion: {subregion}
                            </p>
                            <p>
                                Area: {area} km^2
                            </p>
                            <p>
                                Population: {population} os
                            </p>
                            <p>
                                Timezones:
                            </p>
                            {timezones.length && (
                                <ul>
                                    {timezones.map(timezone => (
                                        <p key={timezone}>
                                            {timezone}
                                        </p>
                                    ))}
                                </ul>
                            )}
                        </>
                    ) : null
                }
            </div>
        )
    }  
}

CountryDetails.contextType = CountryContext

export default CountryDetails
import React, { useContext } from 'react'
import { Map, GoogleApiWrapper } from 'google-maps-react'
import { CountryContext } from './Layout'

const mapStyles = {
  width: '300px',
  height: '300px',
}

const MapContainer = ({ google }) => {
    const {
        selectedCountry: {
            latlng = []
        }
    } = useContext(CountryContext)

    return (
        latlng.length ? (
            <>
                <Map
                    className='map'
                    google={google}
                    zoom={3}
                    style={mapStyles}
                    controlSize="300px"
                    center={{
                        lat: latlng[0],
                        lng: latlng[1]
                    }}
                />
                <style jsx>
                {`
                    .map {
                        width: 300px !important;
                        height: 300px !important;
                    }
                `}
            </style>
            </>
        ) : null
    )
}


export default GoogleApiWrapper({
    apiKey: 'AIzaSyB8zTYYmKYI7CUDE01_Dy_7YWn7kiiIYLQ'
})(MapContainer)
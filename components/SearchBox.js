import React, { useState, useContext } from 'react'
import axios from 'axios'
import { useRouter } from 'next/router'
import { CountryContext } from './Layout'


const SearchBox = () => {
    const router = useRouter()
    const [searchValue, setSearchValue] = useState('')
    const {
        setSelectedCountry
    } = useContext(CountryContext)

    const searchCountry = () => {
        axios.get(`https://restcountries-v1.p.rapidapi.com/name/${searchValue}`)
            .then(({ data }) => {
                setSelectedCountry(data[0])
                router.push("/countries/[cid]", `/countries/${data[0].name.toLowerCase()}`)
            })
            .catch(() => console.log('error'))
    }

    return (
        <div className="search-wrapper">
            <input
                type="text"
                id="search"
                name="search"
                value={searchValue}
                placeholder="Search for..."
                onChange={e => setSearchValue(e.target.value)}
            />
            <button onClick={() => searchCountry()}>
                Search
            </button>
            <style jsx>
                {`
                    .search-wrapper {
                        width: 100%;
                        display: flex;
                        justify-content: space-between;
                        margin: 20px 0;
                    }
                    input {
                        width: 66%;
                        padding: 10px;
                        font-size: 14px;
                    }
                    button {
                        width: 25%;
                        cursor: pointer;
                    }
                `}
            </style>
        </div>  
    )
}

export default SearchBox

import React, { createContext, useState } from 'react'
import Head from 'next/head'
import Hero from './Hero'
import Search from './Search'

export const CountryContext = createContext({})
export const LanguageContext = createContext('en')

const Layout = ({ children }) => {
    const [selectedCountry, setSelectedCountry] = useState({})
    const [selectedLanguage, setSelectedLanguage] = useState('en')
    CountryContext.displayName = 'SelectedCountry'

    return (
        <div>
            <CountryContext.Provider
                value={{
                    setSelectedCountry,
                    selectedCountry
                }}
            >
                <LanguageContext.Provider
                    value={{
                        lang: selectedLanguage,
                        setSelectedLanguage
                    }}
                >
                    <Head>
                        <title>Countries</title>
                        <link rel="icon" href="/favicon.ico" />
                    </Head>
                    <header>
                        <Hero />
                    </header>
                    <main>
                        <Search />
                        {children}
                    </main>
                    <footer>
                        <p>React Context App  - countries</p>
                        <div>
                            <button onClick={() => setSelectedLanguage('en')}>ENG</button>
                            <button onClick={() => setSelectedLanguage('pl')}>POL</button>
                        </div>
                    </footer>
                    <style jsx>
                        {`
                            * {
                                box-sizing: border-box;
                            }
                            :global(a) {
                                color: whitesmoke;
                                text-decoration: none;
                                text-transform: uppercase;
                                font-size: 20px;
                                
                            }
                            :global(body) {
                                margin: 0;
                                font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
                                Helvetica, sans-serif;
                                box-sizing: border-box;
                                max-width: 1000px;
                                margin: 0 auto;
                                
                            }
                            header {
                                height: 10vh;
                                display: flex;
                                align-items: center;
                                justify-content: center;
                                background: darkslategrey;
                            }
                            main {
                                min-height: calc(100vh - 15vh);
                                padding: 60px 40px;
                            }
                            footer {
                                height: 5vh;
                                background: lightgrey;
                                display: flex;
                                align-items: center;
                                justify-content: space-between;
                                color: whitesmoke;
                                text-decoration: none;
                                text-transform: uppercase;
                                font-size: 14px;
                                padding: 0 150px;
                                position: sticky;
                            }
                            footer button {
                                margin-right: 10px;
                                cursor: pointer;
                            }
                        `}
                    </style>
                </LanguageContext.Provider>
            </CountryContext.Provider>
        </div>
    )
}

export default Layout

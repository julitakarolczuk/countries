import React, { useContext } from 'react'
import { CountryContext } from './Layout'

const Banner = () => {    
    const { selectedCountry } = useContext(CountryContext)

    return (
        <>
            {selectedCountry.name && 
                <h1>
                    {selectedCountry.name}
                </h1>}
            <style jsx>
                {`
                    h1 {
                        background: darkgrey;
                        position: absolute;
                        bottom: -90px;
                        position: absolute;
                        width: 100%;
                        text-align: center;
                        padding: 10px 0;
                        color: whitesmoke;
                    }
                `}
            </style>
        </>
    )
}

export default Banner

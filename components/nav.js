import React, { useEffect, useState, useContext } from 'react'
import Link from 'next/link'
import axios from 'axios'
import { CountryContext } from './Layout'

const Nav = () => {
  const [countries, setCountries] = useState([])
  const { setSelectedCountry } = useContext(CountryContext)

  const getThreeRandomCountries = data => {
    let i = 0
    const threeCountries = []
    while (i < 3) {
      threeCountries.push(data[Math.floor(Math.random() * data.length)])
      i++
    }

    return threeCountries
  }
  useEffect(() => {
    axios.defaults.headers.common["x-rapidapi-host"] = "restcountries-v1.p.rapidapi.com"
    axios.defaults.headers.common["x-rapidapi-key"] = "ffe806ba4bmsh63950c5a9d7f5d5p1ed708jsn61bce6cb94c3"
    axios.get('https://restcountries-v1.p.rapidapi.com/all')
      .then(({ data }) => setCountries(getThreeRandomCountries(data)))
  }, [])

  return (
    <nav>
      <ul>
        {countries.map(country => (
          <li
            key={country.name}
            onClick={() => setSelectedCountry(country)}
          >
            <Link
              href="/countries/[cid]"
              as={`/countries/${country.name.toLowerCase()}`}
            >
              {country.name}
            </Link>
          </li>
        ))}
      </ul>
      <style jsx>
        {`
            ul {
              display: flex;
              justify-content: space-evenly;
              padding: 0;
              list-style: none;
            }
            ul li a {
              text-decoration: none;
              color: black;
            }
        `}
      </style>
    </nav>  
  )
}

export default Nav

import React from 'react'
import { CountryContext } from './Layout'

class CountryName extends React.Component {   
    render () {
        return (
            <div>
                <CountryContext.Consumer>
                    {({ selectedCountry }) => 
                        JSON.stringify(selectedCountry) !== '{}' ? (
                            <h1>
                                {selectedCountry.name}
                            </h1>
                        ) : null
                    }
                </CountryContext.Consumer>
            </div>
        )
    } 
}

export default CountryName

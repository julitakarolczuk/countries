import React from 'react'
import { useRouter } from 'next/router'
import Nav from './nav'
import Banner from './Banner'

const Hero = () => {
    const router = useRouter()
    const { cid } = router.query
    
    return (
        <div className="wrapper">
            <Nav />
            {cid && (
                <Banner />
            )}
             <style jsx>
                {`
                    .wrapper {
                        width: 100%;
                        position: relative;
                    }
                `}
            </style>
        </div>  
    )
}

export default Hero
